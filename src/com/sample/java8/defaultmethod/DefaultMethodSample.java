package com.sample.java8.defaultmethod;

public class DefaultMethodSample implements SampleInterface1, SampleInterface2 {

	@Override
	public void method2() {
		System.out.println("This is method from SampleInterface2");

	}

	@Override
	public void method1(String str) {
		System.out.println("This is method from SampleInterface1");

	}
	
	//As log method is there in both SampleInterface1 and SampleInterface2, it will be compilation error
	//To avoid this either override default method from either of interface
	//or create your own method
	@Override
	public void log(String str) {
		// TODO Auto-generated method stub
		SampleInterface2.super.log(str);
	}

}
