package com.sample.java8.defaultmethod;

/*
 * 
An informative annotation type used to indicate that an interface type declaration is intended 
to be a functional interface as defined by the Java Language Specification. 
Conceptually, a functional interface has exactly one abstract method.
 */
@FunctionalInterface
public interface SampleInterface2 {

void method2();
	
	default void log(String str){
		System.out.println("I2 logging::"+str);
	}

}
