package com.sample.java8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ListStreaming {

	public static void main(String ar[]) {

		List<Integer> myList = new ArrayList<>();
		for (int i = 0; i < 10; i++)
			myList.add(i);

		// sequential stream
		Stream<Integer> sequentialStream = myList.stream();

		// parallel stream
		Stream<Integer> parallelStream = myList.parallelStream();
		// Printing the list with lambda expression
		myList.forEach(i -> System.out.println("List values are "+i));

		// filter example for stream for which numbers are greater than 5
		Stream<Integer> highNumsParallel = parallelStream.filter(p -> p > 5);
		highNumsParallel.forEach(p -> System.out.println("Parallel traversing "+p));
		
		//Does this stream contain 7
		highNumsParallel = myList.parallelStream().filter(p -> p > 5);
		System.out.println("Stream contains 7? "+highNumsParallel.anyMatch(i -> i==7));
		
		//Does stream contain number 1 to 5
		highNumsParallel = myList.parallelStream().filter(p -> p > 5);
		System.out.println("Stream contains all elements less than 5? "+highNumsParallel.allMatch(i -> i<5));

		Stream<Integer> highNumSequence = sequentialStream.filter(n -> n > 5);
		highNumSequence.forEach(i -> System.out.println("Sequential traversing "+i));
		
		//Sum of all numbers greated than 5
		
		//earlier approach
		System.out.println("Earlier approach sum: "+sumIterator(myList));
		
		//Sequential stream
		System.out.println("Sequence Stream approach  sum: "+sumStream(myList));
		
		//parallel stream
		System.out.println("parallel Stream approach  sum: "+sumParallelStream(myList));

	}
	
	
	private static int sumIterator(List<Integer> list) {
		Iterator<Integer> it = list.iterator();
		int sum = 0;
		while (it.hasNext()) {
			int num = it.next();
			if (num > 5) {
				sum += num;
			}
		}
		return sum;
	}
	
	private static int sumStream(List<Integer> list) {
		return list.stream().filter(i -> i > 5).mapToInt(i -> i).sum();
	}
	
	private static int sumParallelStream(List<Integer> list) {
		return list.parallelStream().filter(i -> i > 5).mapToInt(i -> i).sum();
	}

}
