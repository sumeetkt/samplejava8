package com.sample.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ForEachExample {

	public static void main(String ar[]) {
		
		//Sample list
		List<Integer> myList = new ArrayList<Integer>();
			for(int i=0; i<10; i++) myList.add(i);
		
		//Foreach using java8 - traversing over myList using lambda expression
			myList.forEach(i -> System.out.println("My List simple traversing " + i));
			
			//Using method reference
			myList.forEach(System.out::println);
			
			//traversing through forEach method of Iterable with anonymous class
			//No class declaration, similar to Java script
			myList.forEach(new Consumer<Integer>() {

				public void accept(Integer t) {
					System.out.println("forEach anonymous class Value::"+t);
				}

			});
			
			
			//traversing with Consumer interface implementation
			MyConsumer consumer = new MyConsumer();
			myList.forEach(consumer);
			
	}		
	}
	
	//Consumer implementation that can be reused
	class MyConsumer implements Consumer<Integer>{

		public void accept(Integer t) {
			System.out.println("Consumer impl Value::"+t);
		}
	
	}

